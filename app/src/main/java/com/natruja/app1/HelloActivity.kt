package com.natruja.app1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TextView

class HelloActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hello)
        val Textname =  intent.getStringExtra("Name")
        val name : TextView = findViewById(R.id.name)
        name.text = Textname
        val Textid = intent.getStringExtra("ID")
        Log.d("NameID",Textname.toString())
        Log.d("NameID",Textid.toString())
    }
}