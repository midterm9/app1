package com.natruja.app1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    var btn1: Button? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val Textname : TextView = findViewById(R.id.Textname)
        val name : String = Textname.text.toString()
        val Textid : TextView = findViewById(R.id.Textid)
        val id : String = Textid.text.toString()
        btn1 = findViewById(R.id.btn)
        btn1?.setOnClickListener{
            val intent = Intent(this,HelloActivity::class.java)
            intent.putExtra("Name",name)
            intent.putExtra("ID",id)
            startActivity(intent)
        }
    }
}